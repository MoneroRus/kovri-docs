# Общее руководство
- Мы стараемся достичь полного соответствия C++11/14. Используйте эту информацию во благо
- Пожалуйста, используйте стандартную библиотеку и библиотеку зависимостей всякий раз, когда это представляется возможным

## Устранение уязвимостей
- Используемый нами [процесс устранения уязвимостей](https://github.com/monero-project/meta/blob/master/VULNERABILITY_RESPONSE_PROCESS.md) предполагает ответственное раскрытие
- С нами также можно связаться через [HackerOne](https://hackerone.com/monero)

## Стиль
1. Прочитайте [Руководство по стилю C++ от Google’s](https://google.github.io/styleguide/cppguide.html) (в частности ссылку по неформатированному стилю)
   - Если вы занимаетесь bash-программированием, ознакомьтесь с [Руководством по стилю оболочки от Google](https://github.com/google/styleguide/blob/gh-pages/shell.xml)
2. В случае с файлами, содержащими только новую работу, используйте [clang-format](http://clang.llvm.org/docs/ClangFormat.html) и ```-style=file``` (использующие наш [.clang-format](https://gitlab.com/kovri-project/kovri/blob/master/.clang-format))
```bash
$ cd kovri/ && clang-format -i -style=file src/path/to/my/file
```
3. В случае с файлами со смешанной (существующей и новой) работой, используйте [clang-format](http://clang.llvm.org/docs/ClangFormat.html) выборочно, только с теми строками, которые напрямую связаны с новой работой
   - Примеры конфигурирования привязки клавиш для плагинов `clang-format` можно найти в документации по [vim](http://clang.llvm.org/docs/ClangFormat.html#vim-integration) и [emacs](http://clang.llvm.org/docs/ClangFormat.html#emacs-integration)
4. Запустите [cpplint](https://github.com/google/styleguide/tree/gh-pages/cpplint) (использующий наш [CPPLINT.cfg](https://gitlab.com/kovri-project/kovri/blob/master/CPPLINT.cfg)). Это позволит выявить все недочёты, которые были пропущены clang-format
```bash
$ cd kovri/ && cpplint src/path/to/my/file && [edit file manually to apply fixes]
```

### Плагины

- Интеграция Vim
  - [clang-format](http://clang.llvm.org/docs/ClangFormat.html#vim-integration)
  - [clang-format ubuntu 16.04 vim workaround](http://stackoverflow.com/questions/39490082/clang-format-not-working-under-gvim)
  - [cpplint.vim](https://github.com/vim-syntastic/syntastic/blob/master/syntax_checkers/cpp/cpplint.vim)
- Интеграция emacs
  - [clang-format](http://clang.llvm.org/docs/ClangFormat.html#emacs-integration) + [clang-format.el](https://llvm.org/svn/llvm-project/cfe/trunk/tools/clang-format/clang-format.el)
  - [flycheck-google-cpplint.el](https://github.com/flycheck/flycheck-google-cpplint)

### Изменения в предлагаемом Google стиле C++

- Избегайте добавления к началу смешанных ```k``` и MACRO_TYPE в случае со всеми константами
- Используйте три слэша Doxygen ```/// C++ comments```в случае документирования комментариев для Doxygen
- Постарайтесь документировать всё, что делаете для Doxygen по мере продвижения работы
- Если вас заботит ваша анонимность, постарайтесь подражать стилю текущей версии.

### Optional Checks
1. Используйте [cppdep](https://github.com/rakhimov/cppdep)
   для проверки зависимости компонента, физической изоляции и включённых проверок. Используйте
2. Используйте [cppcheck](https://github.com/danmar/cppcheck/) для статистического анализа
   (в дополнение к Coverity)
3. Используйте [lizard](https://github.com/terryyin/lizard) для проверки сложности кода

## Отправка вашей работы
Чтобы отправить вашу работу на проверку, сделайте следующее:

1. [Форк](https://help.github.com/articles/fork-a-repo/) Kovri
2. Ознакомьтесь с разделом данного документа, касающимся стилей
3. Создайте [ветку с темой](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)
   - В настоящее время у нас нет каких-либо тегов, так как мы находимся на этапе "преальфа" версии. На данный момент вы можете строить свою работу на основной ветке.
4. Внесение изменений
   - Коммиты должны быть по возможности [атомными](https://en.wikipedia.org/wiki/Atomic_commit#Atomic_commit_convention), а диффы легко читаемыми.
   - Пожалуйста, постарайтесь не смешивать исправления в форматировании с коммитами, не имеющими отношения к форматированию.
5. Соблюдайте правила вежливости в git-log
   - В названии коммита должен быть сначала указан класс или аспект проекта. Например, HTTPProxy: implement User-Agent scrubber. Fixes #193 (HTTPProxy: реализация скруббера Пользователь-Агент. Исправление №193) или Garlic: fix uninitialized padding in ElGamalBlock (Чесночное шифрование: исправление неинициализированного заполнения в ElGamalBlock)
   - Изложение коммита должно быть подробным по умолчанию, состоящим из короткой строки темы (50 символов максимум), пустой строки, а также подробного пояснительного текста отдельным параграфом(-ами), если само по себе название не носит пояснительного характера
   - Если определённый коммит ссылается на другой вопрос, пожалуйста, добавьте ссылку. Например,  *See #123* (См. №123) или *Fixes #123* (Исправление №123). Это поможет разобрать ярлыки при слиянии с `master`
   - Если определённый коммит будет перенесён после обработки пулл-реквеста, пожалуйста, укажите номер пулл-реквеста в сообщении к коммиту. Например, *References #123* (Ссылка №123)
6. [**Подпишите**](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work) ваш коммит(-ы) и, если вы впервые делаете это, создайте новый пулл-реквест, который добавит ваш PGP ключ в наш репозиторий (см. вклад)
7. Отправьте пулл-реквест в `master` ветку
   - В теле пулл-реквеста должно содержаться точное описание того, что делает патч, а также обоснование/указание причины, по которой предлагается патч (если это будет уместно). Следует включить ссылки на любые темы, билеты или чаты на IRC

## Предложения
Чтобы внести предложение, пожалуйста, ознакомьтесь с нашим списком [открытых вопросов](https://gitlab.com/kovri-project/kovri/issues), связанных с существующими предложениями. Если вы не найдёте там темы, предлагаемой вами, [откройте новый вопрос](https://gitlab.com/kovri-project/kovri/issues/new)

Мы просим вас создать предложение по следующим причинам:

1. В результате внесения предложения устанавливается связь
2. Предложение свидетельствует о том, что подающий его уважает вклад всех участников проекта
3. Предложение позволяет без каких-либо препятствий переслать предлагаемое для общего обсуждения
4. Предложение экономит время, если вносящий его работает над подобной характеристикой/вопросом
5. Внесение предложений позволяет избежать недоразумений или казусов, или же позволяет подготовиться к вероятному недоразумению или казусу

*Отсутствие* внесённого предложения *не* умоляет вашего вклада, мы учтём и рассмотрим то, что вы предлагаете. Но крайне рекомендуется делать это путём внесения предложения.

## TODO
- Проведите быстрый поиск по кодовой базе на предмет наличия ```TODO(неназначенных):``` и/или возьмите билет (ticket) и начните работу над патчем!
- Если вы сами создаёте TODO, назначаете задание самому себе или пишете в ```TODO(неназначенные):```

## Fuzz-тестирование

Из [руководства](http://llvm.org/docs/LibFuzzer.html): "LibFuzzer находится на стадии активной разработки, поэтому вам понадобится текущая (или, по крайней мере, самая последняя) версия компилятора Clang"

Последняя версия Clang:

```bash
$ cd ~/ && mkdir TMP_CLANG && git clone https://chromium.googlesource.com/chromium/src/tools/clang TMP_CLANG/clang
$ ./TMP_CLANG/clang/scripts/update.py
$ cd --
```

Последняя версия libFuzzer:

```bash
$ git clone https://chromium.googlesource.com/chromium/llvm-project/llvm/lib/Fuzzer contrib/Fuzzer
```

Построение Kovri с активным fuzz-тестированием:

```bash
$ PATH="~/third_party/llvm-build/Release+Asserts/bin:$PATH" CC=clang CXX=clang++ make fuzz-tests
```

Использование (пример для RouterInfo):

```bash
mkdir RI_CORPUS MIN_RI_CORPUS
find ~/.kovri/core/network_database/ -name "router_info*" -exec cp {} RI_CORPUS \;
./build/kovri-util fuzz --target=routerinfo -merge=1 MIN_RI_CORPUS RI_CORPUS
./build/kovri-util fuzz --target=routerinfo -jobs=2 -workers=2 MIN_RI_CORPUS
```

# Контроль качества

Далее предлагается модель последовательности действий процесса контроля качества. Несмотря на линейность, любой этап может быть реализован индивидуально, так как в конечном счёте реализуются все этапы.

## Этап 1. Базовая проверка

- Проверка открытых вопросов посредством нашего [Issue Tracker](https://gitlab.com/kovri-project/kovri/issues/)
- Проверка нашего [процесса устранения уязвимостей](https://github.com/monero-project/meta/blob/master/VULNERABILITY_RESPONSE_PROCESS.md)
- Весь код должен соответствовать нашим руководствам по подаче
- Выявление областей, требующих улучшения (смыслового или в коде)
- Создание TODO и назначение исполнителя, если это будет возможно

## Этап 2. Проверка спецификации / реализация / документирование кода

- Проверка спецификации модуль за модулем, например, Streaming, I2PControl, и т. д.
  - Код должен соответствовать важным частям спецификации, обеспечивая тот же (или более высокий) уровень анонимности, чем java I2P
  - Изменить коэффициенты/реализовать/произвести патч, когда/где это будет необходимо
- Обеспечить реализацию, соответствующую C++11/14
  - Проверить этап 2 при необходимости
- Решить все соответствующие TODO
- Максимально, насколько это возможно, задокументировать код в соответствии с комментариями и Doxygen
  - Код должен быть понятен как новичкам, так и опытным программистам
  - Код должен подвести читателя к лучшему пониманию I2P
    - I2P является очень сложным протоколом, поэтому наш код должен работать как независимая замена нормативной документации, а не просто как некое дополнение (возможно, это трудоёмкая задача, но её решение сулит преимущества с точки зрения сопровождения и долговечности программного обеспечения)

## Этап 3. Проверка криптографии / аудит безопасности

- Следует убедиться в том, что криптография актуальна и реализована надлежащим образом
- Необходимо установить вектор для известных вариантов взлома
  - Следует помнить об этих векторах при написании тестов
- Попытаться поломать Kovri всеми возможными способами
  - Исправьте то, что вы поломали
- Всегда по возможности использовать проверенные, качественно написанные библиотеки
  - Избегать доморощенного, специализированного подхода, вроде *"я уверен, что мне лучше, чем сообществу, знать, как написать этот код"*
- Перед тем, как перейти к следующему этапу, найти другое или несколько других мнений среди ваших коллег

## Этап 4. Устранение багов / тестирование / профилирование

- Устранение приоритетных багов/вопросов
- Написание тестов для каждого отдельного модуля
  - Запуск тестов. Повторное их проведение
  - Полная проверка результатов тестирования. Реализация патчей по необходимости. Изменение коэффициентов, если будет нужно
- Следует убедиться в том, что автоматизация работает на регулярной основе
  - valgrind, doxygen, clang-format
  - Реализация патчей по необходимости. Изменение коэффициентов, если будет нужно.

## Этап 5. Обсуждение

- Обсуждение с коллегами и сообществом
  - Обсуждение должно происходить публично, путём публикации билета "ticket", проведения встречи и/или по каналу IRC
- Принятие всех замечаний и реализация ощутимых результатов в ответ.
- При наличии удовлетворительных результатов следует перейти к следующему этапу, либо повторить этот этап (или начать с предыдущего этапа)

## Этап 6. Повторить весь цикл с самого начала
