# لذا ، قمت بتثبيت كوفري. ماذا الآن؟

## خطوه 1. افتح NAT/Firewall
1. اختر منفذًا بين `` 9111``` و `` `30777``
2. ** احفظ هذا المنفذ في ملف التكوين الخاص بك ** (`kovri.conf`)
3. السماح في NAT/Firewall للسماح باتصالات TCP / UDP الواردة إلى هذا المنفذ (راجع الملاحظات أدناه إذا لم يكن لديك حق الوصول)

ملاحظات:

- إذا لم تقم بحفظ المنفذ ، فسيقوم كوفري بشكل عشوائي بإنشاء منفذ جديد في كل بدء تشغيل (لديك أيضًا خيار تمرير المنفذ مع علامة `--port` في كل عملية بدء تشغيل).
- إذا لم يكن لديك إمكانية الوصول إلى NAT الخاص بك ، فاستخدم خيار تشغيل --enable upnp` أو قم بتمكين الخيار في `kovri.conf`
- ** لا تشارك رقم المنفذ الخاص بك مع أي شخص لأنه سيؤثر على هويتك! **

## الخطوة 2. (مستحسن) الأمن التشغيلي

- فكر في إنشاء مستخدم "كوفري" معين وتشغيل كوفري باستخدام ذلك المستخدم فقط
- في حالة استخدام لينكس ، فكر في استخدام kernel متصلب (مثل [grsec] (https://en.wikibooks.org/wiki/Grsecurity) باستخدام RBAC)
- بعد تثبيت الموارد المناسبة في مسار بيانات kovri ، مع مراعاة ضبط التحكم في الوصول المناسب مع [setfacl] (https://linux.die.net/man/1/setfacl) ، [umask] (https: //en.wikipedia .org / wiki / Umask) ، أو أيًا كان نظام التشغيل الخاص بك يستخدم لـ ACL
- لا تشارك أبداً رقم المنفذ الخاص بك مع أي شخص لأنه يؤثر على هويتك!

** ملاحظة: راجع kovri.conf للعثور على مسار البيانات الخاص بك لـ Linux / OSX / Windows **

## الخطوة 3. تكوين كوفري

للحصول على قائمة كاملة بالخيارات:

```bash
# Linux / macOS / *BSD
$ cd ~/bin && ./kovri --help
```

```bash
# Windows (PowerShell / MSYS2)
$ cd "C:\Program Files\Kovri" ; ./kovri.exe --help
```

للحصول على خيارات كاملة مع التفاصيل:

- `kovri.conf` ملف التكوين لجهاز التوجيه والعميل
- `tunnels.conf` ملف التكوين لأنفاق العميل / الخادم

## الخطوة 4. (اختياري) إعداد الأنفاق

باختصار ، * * الأنفاق الخاصة بالعميل * هي الأنفاق التي تستخدمها للاتصال بالخدمات الأخرى و * أنفاق الخادم * يتم استخدامها عندما تستضيف خدمة (خدمات) بحيث يمكن لأشخاص آخرين الاتصال بخدمتك (الموقع ، SSH ، إلخ).

بشكل افتراضي ، سيكون لديك إعداد أنفاق العميل لـ IRC (Irc2P) والبريد الإلكتروني (i2pmail). لإضافة / إزالة أنفاق العميل ، راجع `tunnels.conf`.

عند إنشاء نفق (نطاقات) الخادم ، ستحتاج إلى إنشاء * مفاتيح خاصة دائمة * يتم استخدامها لوجهتك. للقيام بذلك ، قم بتفكيك أو إنشاء "keys = your-keys.dat` واستبدل" مفاتيحك "باسم مناسب. ** لا تشارك ملفك الخاص ".dat` مع أي شخص (الاستثناء الوحيد هو إذا كنت ترغب في نشر التعدد) ، وتأكد من عمل نسخة احتياطية من الملف! **

بمجرد الإعداد ، سيظهر [Base32] (https://getmonero.org/resources/moneropedia/base32-address) و [Base64] (https://getmonero.org/resources/moneropedia/base64-address) الوجهة المشفرة في السجل الخاص بك بعد بدء kovri. يمكنك أيضًا العثور على هذه الترميزات في ملف نصي جنبًا إلى جنب مع ملف `.dat` في مسار بيانات kovri في دليل` العميل / المفاتيح`. إن الوجهة المشفرة داخل الملفات النصية ** .dat.b32.txt و `.dat.b64.txt` آمنة للتوزيع على الآخرين حتى يتمكنوا من الاتصال بالخدمة الخاصة بك.
مثال:

- ملف المفاتيح الخاصه: `client/keys/your-keys.dat`
- العامه [Base32](https://getmonero.org/resources/moneropedia/base32-address): `client/keys/your-keys.dat.b32.txt`
- العامه [Base64](https://getmonero.org/resources/moneropedia/base64-address): `client/keys/your-keys.dat.b64.txt`

** ملاحظة: راجع kovri.conf للعثور على مسار البيانات الخاص بك لـ Linux / OSX / Windows **

## الخطوة 5. (اختياري) قم بتسجيل [eepsite] الجديد الخاص بك (https://getmonero.org/resources/moneropedia/eepite)

**توقف! حتى يتم حل [# 498] (https://gitlab.com/kovri-project/kovri/issues/498) ، فكر فقط في تسجيل خدمتك باستخدام كوفري و * ليس * stats.i2p!**

- افتح طلباً مع `[Subscription Request] your-host.i2p` (استبدل  your-host.i2p مع اسمك المفضل) في [متتبع اعطال كوفري](https://gitlab.com/kovri-project/kovri/issues)
- في نص الطلب ، الصق محتويات ملف `.txt` العام الذي تمت الإشارة إليه في الخطوة السابقة
- بعد المراجعة ، سنضيف مضيفك ونوقع الاشتراك
- تم!

## Step 6. شغل كوفري
```bash
$ cd build/ && ./kovri
```
- انتظر 5 دقائق أو حتى يتم ربطك في الشبكة قبل محاولة استخدام الخدمات

## Step 7. شاركنا في قناه IRC
1. شغل [IRC عميل](https://en.wikipedia.org/wiki/List_of_IRC_clients)
2. إعداد العميل للاتصال منفذ IRC كوفري (الافتراضي 6669). هذا سوف يوصلك بشبكة Irc2P (شبكة IRC في I2P)
3. انضم الي `#kovri` و `#kovri-dev`

## Step 8. تصفح موقع I2P (garlic-site/eepsite)
1. بدء تشغيل متصفح من اختيارك (يفضل متصفح مخصص لاستخدام كوفري)
2. قم بتكوين المستعرض الخاص بك عن طريق قراءة [هذه الإرشادات] (https://geti2p.net/en/about/browser-config) ** ولكن بدلاً من المنفذ 4444 و 4445 ** غيّر منفذ HTTP الوكيل إلى ** 4446 ** و SSL proxy port * * * الي ** 4446 **
3. تفضل بزيارة http: //check.kovri.i2p

ملاحظات:

- ** تمامًا كما هو الحال مع Tor ، لا يحتاج المرء إلى SSL لاستخدام الشبكة بشكل آمن **
- لا يتم حاليًا تنفيذ دعم موقع طبقة المقابس الآمنة وخدمة outproxy
- إذا أعطاك أحد الأشخاص عنوان .i2p غير موجود في دفتر عناوينك ، فاستخدم خدمة `Jump` في http: //stats.i2p/i2p/lookup.html
- ابحث خلال hosts.txt في دليل البيانات الخاص بك لعرض قائمة بالمواقع الافتراضية التي يمكنك زيارتها بسهولة
- بشكل عام ، تم تطوير تطبيق وكيل HTTP وبروتوكول دفتر العناوين ولم يكتمل بعد

## الخطوة 9. استمتع!
- اقرأ المزيد عن Kovri في [مونيروبيديا] (https://getmonero.org/resources/moneropedia/kovri).
- افتح طلباتك الخاصة أو الإبلاغ عن الأخطاء على [تعقب المشكلات] لدينا (https://gitlab.com/kovri-project/kovri/issues)
- اعرف المزيد عن شبكة I2P على [java I2P موقع] (https://geti2p.net/en/docs)

# خيارات الحاويات

## Snapcraft

في أنظمة لينكس ، استخدم snapcraft لسهولة النشر.

### Step 1. نسخ مستودع المصدر

```bash
$ git clone --recursive https://gitlab.com/kovri-project/kovri
```

### Step 2. تنصيب snapcraft

- ارجع إلى مدير الحزم الخاص بالتوزيع الخاص بك snapcraft and [snapd](https://snapcraft.io/docs/core/install)

On Ubuntu, simple run:
```bash
$ sudo apt-get install snapcraft
```

### Step 3. إنشاء snap

```bash
$ cd kovri/ && snapcraft && sudo snap install *.snap --dangerous
```
ملاحظة: - هناك حاجة إلى --dangerous فقط لأنه لم يتم توقيع الخاطف (قمت ببنائه بنفسك ، لذا لا يجب أن يكون هذا مشكلة)

### Step 4. شغل كوفري مع snapcraft

```bash
$ snap run kovri
```

## Docker

### Step 1. تنصيب Docker
تثبيت Docker يقع خارج نطاق هذا المستند ، يرجى الاطلاع على [docker documentation](https://docs.docker.com/engine/installation/)

### Step 2. تكوين / فتح جدار الحماية


تأتي صوره docker مع كوفري افتراضياً ولكن يمكن إعدادها كما شرح سابقاً .

يجب عليك إختيار منفذ عشوائي وفتحه ( شاهد الأقسام السابقه)

### Step 3. يعمل

#### الإعدادات الإفتراضيه
```bash
KOVRI_PORT=42085 && sudo docker run -p 127.0.0.1:4446:4446 -p 127.0.0.1:6669:6669 -p $KOVRI_PORT --env KOVRI_PORT=$KOVRI_PORT geti2p/kovri
```

#### الإعدادات الخاصه
حيث `./kovri-settings/` تحوي `kovri.conf` و `tunnels.conf`.
```bash
KOVRI_PORT=42085 && sudo docker run -p 127.0.0.1:4446:4446 -p 127.0.0.1:6669:6669 -p $KOVRI_PORT --env KOVRI_PORT=$KOVRI_PORT -v kovri-settings:/home/kovri/.kovri/config:ro geti2p/kovri
```
