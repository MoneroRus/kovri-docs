[كوفري](https://getmonero.org/resources/moneropedia/kovri.html) هو تقنيه تخفي لامركزيه و مجانيه مطوره من قبل  [مونيرو](https://getmonero.org).

حالياً مبني علي مواصفات شبكه [I2P](https://getmonero.org/resources/moneropedia/i2p.html), يستخدم كوفري كلاً من [garlic تشفير](https://getmonero.org/resources/moneropedia/garlic-encryption.html) and [garlic توجيه ](https://getmonero.org/resources/moneropedia/garlic-routing.html) لإنشاء شبكه متراكبه خاصه محميه عبر الإنترنت, هذه الشبكه المتراكبه توفر لمستخدميها إمكانيه إخفاء موقعهم الجغرافي وعناوين الإنترنت الخاصه بهم بشكل فعال.

بشكل اساسي يعمل كوفري علي إخفاء حركة مرور الإنترنت الخاصة بالتطبيق لجعله مجهولاً ضمن الشبكة.

كوفري هو مُوجّه خفيف الوزن يهدف لحفظ الخصوصيه متوافق تماما مع شبكة I2P.

شاهد التطوير من خلال [مستودع كوفري](https://gitlab.com/kovri-project/kovri#downloads) و [الانضمام إلى المجتمع](https://gitlab.com/kovri-project/kovri#contact).
